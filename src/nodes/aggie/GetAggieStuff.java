package nodes.aggie;

import org.dreambot.api.methods.MethodProvider;

import dream_bot_ivars.Node;
import dream_bot_ivars.main;

public class GetAggieStuff extends Node {
	public boolean possible = true;

	public GetAggieStuff(main c) {
		super(c);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean validate() {
		if (!c.getInventory().contains(item -> item != null && item.getName().contains(" leaf"))
				|| !c.getInventory().contains(item -> item != null && item.getName().contains("oins"))) {
			return true;
		}
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int execute() {
		// TODO Auto-generated method stub
		if ((c.getBank().isOpen()) && (c.getBank().contains(item -> item != null && item.getName().contains(" leaf"))
				&& c.getBank().contains(item -> item != null && item.getName().contains("oins")))) {
			c.getBank().depositAllItems();
			c.getBank().withdrawAll(item -> item != null && item.getName().contains(" leaf"));
			c.getBank().withdrawAll(item -> item != null && item.getName().contains("oins"));

		} else if ((c.getBank().isOpen()) && (!c.getBank()
				.contains(item -> item != null && item.getName().contains(" leaf"))
				|| !c.getBank().contains(item -> item != null && item.getName().contains("oins"))
						&& (!c.getInventory().contains(item -> item != null && item.getName().contains(" leaf")) || !c
								.getInventory().contains(item -> item != null && item.getName().contains("oins"))))) {
			MethodProvider.log("cant do Aggie");
			possible = false;

		} else {
			c.getBank().openClosest();
		}
		MethodProvider.log("doing Aggie");
		return 1000;
	}

	@Override
	public boolean possible() {
		// TODO Auto-generated method stub
		return possible;
	}

}
