package nodes.aggie;

import org.dreambot.api.methods.map.Area;

import dream_bot_ivars.Node;
import dream_bot_ivars.main;

public class WalkNodeAggie extends Node {
	private static final Area aggie_area = new Area(3083, 3256, 3088, 3260, 0);
	public boolean possible = true;

	public WalkNodeAggie(main c) {
		super(c);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean validate() {

		// TODO Auto-generated method stub
		if (!aggie_area.contains(c.getLocalPlayer()) && (int) (Math.random() * 3 + 1) == 3) {
			return true;
		} else if (!aggie_area.contains(c.getLocalPlayer()) && !c.getLocalPlayer().isMoving()) {
			return true;
		}
		return false;
	}

	@Override
	public int execute() {
		// TODO Auto-generated method stub
		c.getWalking().walk(aggie_area.getRandomTile());
		return 1000;
	}

	@Override
	public boolean possible() {
		// TODO Auto-generated method stub
		return possible;
	}

}
