package nodes.aggie;

import java.awt.Point;

import org.dreambot.api.methods.Calculations;
import org.dreambot.api.methods.MethodProvider;
import org.dreambot.api.methods.input.mouse.MouseSettings;

import dream_bot_ivars.Node;
import dream_bot_ivars.main;

public class AggieNode extends Node {
	public boolean possible = true;

	public AggieNode(main c) {
		super(c);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		if (!c.getInventory().contains("Woad leaf") || !c.getInventory().contains("Coins")) {
			return false;
		}
		if (c.getLocalPlayer().isMoving()) {
			MethodProvider.log("walking");
			return false;
		}
		return true;
	}

	@Override
	public int execute() {
		MouseSettings.setSpeed(Calculations.random(1, 10));
		c.getNpcs().closest("Aggie").interact("Talk-to");
		MethodProvider.sleep(1500, 2000);
		if (c.getLocalPlayer().isInteractedWith()) {
			c.getMouse().move(new Point(Calculations.random(0, 765), Calculations.random(0, 503))); // antiban
			MethodProvider.sleep(700, 1000);
			c.getMouse().click(new Point(Calculations.random(238, 381), Calculations.random(447, 454)));
			MethodProvider.sleep(700, 1000);
			c.getMouse().click(new Point(Calculations.random(149, 373), Calculations.random(441, 453)));
			MethodProvider.sleep(700, 1000);
			c.getMouse().click(new Point(Calculations.random(144, 284), Calculations.random(445, 456)));
			MethodProvider.sleep(700, 1000);
			c.getMouse().click(new Point(Calculations.random(233, 279), Calculations.random(445, 456)));
			MethodProvider.sleep(700, 1000);
			c.getMouse().click(new Point(Calculations.random(131, 384), Calculations.random(435, 445)));
			MethodProvider.sleep(700, 1000);
			c.getMouse().click(new Point(Calculations.random(228, 372), Calculations.random(446, 456)));
			MethodProvider.sleep(700, 1000);
			c.getMouse().click(new Point(Calculations.random(228, 372), Calculations.random(446, 456)));
			MethodProvider.sleep(700, 1000);
			c.getMouse().click(new Point(Calculations.random(139, 379), Calculations.random(382, 390)));
			MethodProvider.sleep(700, 1000);
			c.getMouse().click(new Point(Calculations.random(148, 286), Calculations.random(444, 457)));

		}
		// TODO Auto-generated method stub
		return 1000;
	}

	@Override
	public boolean possible() {
		// TODO Auto-generated method stub
		return possible;
	}

}
