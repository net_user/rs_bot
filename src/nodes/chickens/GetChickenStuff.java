package nodes.chickens;

import org.dreambot.api.methods.MethodProvider;

import dream_bot_ivars.Node;
import dream_bot_ivars.main;

public class GetChickenStuff extends Node {
	public boolean possible = true;

	public GetChickenStuff(main c) {
		super(c);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean validate() {
		if (!c.getInventory().contains(item -> item != null && item.getName().contains(" sword"))
				|| c.getEquipment().contains(item -> item != null && item.getName().contains(" sword"))) {
			return true;
		}

		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int execute() {
		if ((c.getBank().isOpen())
				&& (c.getBank().contains(item -> item != null && item.getName().contains(" sword")))) {
			c.getBank().depositAllItems();
			c.getBank().withdraw(item -> item != null && item.getName().contains(" sword"));
		} else if ((c.getBank().isOpen())
				&& (!c.getBank().contains(item -> item != null && item.getName().contains(" sword"))
						|| !c.getInventory().contains(item -> item != null && item.getName().contains(" sword")))
				|| c.getEquipment().contains(item -> item != null && item.getName().contains(" sword"))) {
			MethodProvider.log("cant do chickens");
			possible = false;
		} else {
			c.getBank().openClosest();
		}
		// TODO Auto-generated method stub
		MethodProvider.log("doing chickens");
		return 1000;
	}

	@Override
	public boolean possible() {
		// TODO Auto-generated method stub
		return possible;
	}

}
