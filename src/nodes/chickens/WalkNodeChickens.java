package nodes.chickens;

import org.dreambot.api.methods.map.Area;

import dream_bot_ivars.Node;
import dream_bot_ivars.main;

public class WalkNodeChickens extends Node {
	private static final Area chicken_area = new Area(3235, 3290, 3226, 3301, 0);

	public WalkNodeChickens(main c) {
		super(c);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		if (!chicken_area.contains(c.getLocalPlayer()) && (int) (Math.random() * 3 + 1) == 3) {
			return true;
		} else if (!chicken_area.contains(c.getLocalPlayer()) && !c.getLocalPlayer().isMoving()) {
			return true;
		}
		return false;
	}

	@Override
	public int execute() {
		// TODO Auto-generated method stub
		c.getWalking().walk(chicken_area.getRandomTile());
		return 1000;
	}

	@Override
	public boolean possible() {
		// TODO Auto-generated method stub
		return true;
	}

}
