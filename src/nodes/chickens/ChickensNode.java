package nodes.chickens;

import java.awt.Point;

import org.dreambot.api.methods.Calculations;

import dream_bot_ivars.Node;
import dream_bot_ivars.main;

public class ChickensNode extends Node {
	public boolean possible = true;

	public ChickensNode(main c) {
		super(c);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean validate() {
		if (!c.getInventory().contains(item -> item != null && item.getName().contains(" sword"))
				|| c.getEquipment().contains(item -> item != null && item.getName().contains(" sword"))) {
			return false;
		} else if (c.getLocalPlayer().isInCombat()) {
			return false;
		} else if (c.getLocalPlayer().isMoving()) {
			return false;
		}

		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public int execute() {
		// TODO Auto-generated method stub
		c.getNpcs().closest("Chicken").interact("Attack");
		c.getMouse().move(new Point(Calculations.random(0, 765), Calculations.random(0, 503))); // antiban
		return 1000;
	}

	@Override
	public boolean possible() {
		// TODO Auto-generated method stub
		return possible;
	}

}
