package nodes.woodCutting;

import org.dreambot.api.methods.map.Area;
import org.dreambot.api.methods.skills.Skill;

import dream_bot_ivars.Node;
import dream_bot_ivars.main;

public class WalkNodeWoodCutting extends Node {
	private static final Area tree_area = new Area(3185, 3253, 3207, 3239, 0);
	private static final Area oak_area = new Area(3284, 3413, 3275, 3441, 0);
	// private static final Area willow_area = new Area(3064, 3256, 3056, 3251, 0);
	private static final Area yew_area = new Area(3048, 3275, 3060, 3268, 0);
	public boolean possible = true;

	public WalkNodeWoodCutting(main c) {
		super(c);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		if (c.getSkills().getRealLevel(Skill.WOODCUTTING) < 15) {
			if (!tree_area.contains(c.getLocalPlayer()) && (int) (Math.random() * 3 + 1) == 3) {
				return true;
			} else if (!tree_area.contains(c.getLocalPlayer()) && !c.getLocalPlayer().isMoving()) {
				return true;
			}
		} else if (c.getSkills().getRealLevel(Skill.WOODCUTTING) < 30
				&& (c.getSkills().getRealLevel(Skill.WOODCUTTING) > 14)) {
			if (!oak_area.contains(c.getLocalPlayer()) && (int) (Math.random() * 3 + 1) == 3) {
				return true;
			} else if (!oak_area.contains(c.getLocalPlayer()) && !c.getLocalPlayer().isMoving()) {
				return true;
			}
		} else if (c.getSkills().getRealLevel(Skill.WOODCUTTING) < 61
				&& (c.getSkills().getRealLevel(Skill.WOODCUTTING) > 29)) {
			if (!oak_area.contains(c.getLocalPlayer()) && (int) (Math.random() * 3 + 1) == 3) {
				return true;
			} else if (!oak_area.contains(c.getLocalPlayer()) && !c.getLocalPlayer().isMoving()) {
				return true;
			}
		} else if (c.getSkills().getRealLevel(Skill.WOODCUTTING) > 59) {
			if (!yew_area.contains(c.getLocalPlayer()) && (int) (Math.random() * 3 + 1) == 3) {
				return true;
			} else if (!yew_area.contains(c.getLocalPlayer()) && !c.getLocalPlayer().isMoving()) {
				return true;
			}
		}

		return false;
	}

	@Override
	public int execute() {
		// TODO Auto-generated method stub

		if (c.getSkills().getRealLevel(Skill.WOODCUTTING) < 15) {
			c.getWalking().walk(tree_area.getRandomTile());
		} else if (c.getSkills().getRealLevel(Skill.WOODCUTTING) < 30
				&& (c.getSkills().getRealLevel(Skill.WOODCUTTING) > 14)) {
			c.getWalking().walk(oak_area.getRandomTile());
		} else if (c.getSkills().getRealLevel(Skill.WOODCUTTING) < 61
				&& (c.getSkills().getRealLevel(Skill.WOODCUTTING) > 29)) {
			c.getWalking().walk(oak_area.getRandomTile());
		} else if (c.getSkills().getRealLevel(Skill.WOODCUTTING) > 59) {
			c.getWalking().walk(yew_area.getRandomTile());
		}

		return 500;
	}

	@Override
	public boolean possible() {
		// TODO Auto-generated method stub
		return possible;
	}

}
