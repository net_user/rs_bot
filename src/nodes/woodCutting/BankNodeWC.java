package nodes.woodCutting;

import dream_bot_ivars.Node;
import dream_bot_ivars.main;

public class BankNodeWC extends Node {
	public boolean possible = true;

	public BankNodeWC(main c) {
		super(c);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return c.getInventory().isFull();
	}

	@Override
	public int execute() {
		// TODO Auto-generated method stub
		if (c.getBank().isOpen()) {
			c.getBank().depositAllExcept(item -> item != null && item.getName().contains(" axe"));

		} else {
			c.getBank().openClosest();
		}
		return 500;
	}

	@Override
	public boolean possible() {
		// TODO Auto-generated method stub
		return possible;
	}

}
