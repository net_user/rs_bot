package nodes.woodCutting;

import java.awt.Point;

import org.dreambot.api.methods.Calculations;
import org.dreambot.api.methods.MethodProvider;
import org.dreambot.api.methods.skills.Skill;

import dream_bot_ivars.Node;
import dream_bot_ivars.main;

public class WoodcuttingNode extends Node {
	public boolean possible = true;

	public WoodcuttingNode(main c) {
		super(c);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		if (c.getLocalPlayer().isAnimating()) {
			return false;
		} else if (c.getLocalPlayer().isMoving()) {
			return false;
		}
		return true;
	}

	@Override
	public int execute() {
		String treeFromLvl = "";
		MethodProvider.log(String.valueOf(c.getSkills().getRealLevel(Skill.WOODCUTTING)));
		if (c.getSkills().getRealLevel(Skill.WOODCUTTING) < 15) {
			treeFromLvl = "Tree";
		} else if (c.getSkills().getRealLevel(Skill.WOODCUTTING) < 30
				&& (c.getSkills().getRealLevel(Skill.WOODCUTTING) > 14)) {
			treeFromLvl = "Oak";
		} else if (c.getSkills().getRealLevel(Skill.WOODCUTTING) < 61
				&& (c.getSkills().getRealLevel(Skill.WOODCUTTING) > 29)) {
			treeFromLvl = "Oak";
		} else if (c.getSkills().getRealLevel(Skill.WOODCUTTING) > 59) {
			treeFromLvl = "Yew";
		}
		c.getGameObjects().closest(treeFromLvl).interact("Chop down");
		c.getMouse().move(new Point(Calculations.random(0, 765), Calculations.random(0, 503))); // antiban
		// TODO Auto-generated method stub
		return 1000;
	}

	@Override
	public boolean possible() {
		// TODO Auto-generated method stub
		return possible;
	}

}
