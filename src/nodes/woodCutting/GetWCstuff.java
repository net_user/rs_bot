package nodes.woodCutting;

import org.dreambot.api.methods.MethodProvider;

import dream_bot_ivars.Node;
import dream_bot_ivars.main;

public class GetWCstuff extends Node {
	public boolean possible = true;

	public GetWCstuff(main c) {
		super(c);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean validate() {
		if (!c.getInventory().contains(item -> item != null && item.getName().contains(" axe"))) {
			return true;
		}
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int execute() {
		if ((c.getBank().isOpen()) && (c.getBank().contains(item -> item != null && item.getName().contains(" axe")))) {
			c.getBank().depositAllItems();
			c.getBank().withdraw(item -> item != null && item.getName().contains(" axe"));
		} else if ((c.getBank().isOpen())
				&& (!c.getBank().contains(item -> item != null && item.getName().contains(" axe"))
						|| !c.getInventory().contains(item -> item != null && item.getName().contains(" axe")))) {
			MethodProvider.log("cant do wc");
			possible = false;
		} else {
			c.getBank().openClosest();
		}
		// TODO Auto-generated method stub
		MethodProvider.log("doing WC");
		return 1000;
	}

	@Override
	public boolean possible() {
		// TODO Auto-generated method stub
		return possible;
	}

}
