package nodes.random;

import java.awt.Point;

import org.dreambot.api.methods.Calculations;
import org.dreambot.api.methods.MethodProvider;
import org.dreambot.api.methods.input.mouse.MouseSettings;

import dream_bot_ivars.Node;
import dream_bot_ivars.main;

public class RandomNode extends Node {
	public boolean hovering = false;
	public boolean possible = true;

	public RandomNode(main c) {
		super(c);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public int execute() {
		// TODO Auto-generated method stub
		MouseSettings.setSpeed(Calculations.random(1, 10));
		if ((int) (Math.random() * 15 + 1) == 1) {

			c.getMouse().move(new Point(Calculations.random(0, 765), Calculations.random(0, 503))); // antiban
		} else if ((int) (Math.random() * 6 + 1) == 1) {
			c.getMouse().moveMouseOutsideScreen();
		} else if ((int) (Math.random() * 15 + 1) == 1) {
			c.getCamera().rotateTo(Calculations.random(2400), Calculations.random(c.getClient().getLowestPitch(), 384));

		} else if (!hovering && c.getLocalPlayer().isAnimating() && (int) (Math.random() * 3 + 1) == 2) {
			c.getMouse().click(new Point(Calculations.random(563, 588), Calculations.random(173, 195)));
			c.getMouse().move(new Point(Calculations.random(686, 729), Calculations.random(374, 392)));
			MethodProvider.sleep(10000, 20000);
			hovering = true;
			c.getMouse().click(new Point(Calculations.random(635, 650), Calculations.random(170, 192)));
			c.getMouse().move(new Point(Calculations.random(0, 765), Calculations.random(0, 503))); // antiban
		} else if ((int) (Math.random() * 50 + 1) == 1) {
			hovering = false;
		} else if (!hovering && c.getLocalPlayer().isAnimating() && (int) (Math.random() * 7 + 1) == 1) {
			c.getMouse().click(new Point(Calculations.random(599, 623), Calculations.random(173, 195)));
			c.getMouse().click(new Point(Calculations.random(721, 730), Calculations.random(248, 435)));
			c.getMouse().click(new Point(Calculations.random(565, 592), Calculations.random(243, 449)));
			MethodProvider.sleep(9000, 15000);
			hovering = true;
			c.getMouse().click(new Point(Calculations.random(447, 466), Calculations.random(60, 77)));
			c.getMouse().click(new Point(Calculations.random(630, 655), Calculations.random(174, 196)));

		} else if (!hovering && c.getLocalPlayer().isAnimating() && (int) (Math.random() * 7 + 1) == 3) {
			c.getMouse().click(new Point(Calculations.random(694, 719), Calculations.random(171, 195)));
			c.getMouse().move(new Point(Calculations.random(557, 725), Calculations.random(225, 421)));
			MethodProvider.sleep(5000, 8000);
			hovering = true;
			c.getMouse().click(new Point(Calculations.random(630, 655), Calculations.random(174, 196)));

		} else if (!hovering && c.getLocalPlayer().isAnimating() && (int) (Math.random() * 7 + 1) == 4) {
			c.getMouse().click(new Point(Calculations.random(531, 553), Calculations.random(175, 189)));
			MethodProvider.sleep(3000, 5000);
			hovering = true;
			c.getMouse().click(new Point(Calculations.random(630, 655), Calculations.random(174, 196)));

		}
		return 1000;
	}

	@Override
	public boolean possible() {
		// TODO Auto-generated method stub
		return possible;
	}

}
