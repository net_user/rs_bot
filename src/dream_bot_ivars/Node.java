package dream_bot_ivars;

public abstract class Node {
	protected final main c;

	public Node(main c) {
		this.c = c;
	}

	public abstract boolean validate();

	public abstract int execute();

	public abstract boolean possible();
}
