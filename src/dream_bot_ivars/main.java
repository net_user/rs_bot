package dream_bot_ivars;

import java.util.ArrayList;
import java.util.Date;

import org.dreambot.api.methods.Calculations;
import org.dreambot.api.methods.MethodProvider;
import org.dreambot.api.script.AbstractScript;
import org.dreambot.api.script.Category;
import org.dreambot.api.script.ScriptManifest;

import nodes.aggie.AggieNode;
import nodes.aggie.BankNodeAggie;
import nodes.aggie.GetAggieStuff;
import nodes.aggie.WalkNodeAggie;
import nodes.chickens.ChickensNode;
import nodes.chickens.GetChickenStuff;
import nodes.chickens.WalkNodeChickens;
import nodes.random.RandomNode;
import nodes.woodCutting.BankNodeWC;
import nodes.woodCutting.GetWCstuff;
import nodes.woodCutting.WalkNodeWoodCutting;
import nodes.woodCutting.WoodcuttingNode;

@ScriptManifest(author = "Ivars_p", name = "wc-aggie", version = 1.1, description = " test_app", category = Category.WOODCUTTING)
public class main extends AbstractScript {
	private Node[] currentNodes;
	ArrayList<Node[]> nodeList = new ArrayList<Node[]>();
	Date time = new java.util.Date();
	long currTime;

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		nodeList.add(new Node[] { new GetAggieStuff(this), new BankNodeAggie(this), new WalkNodeAggie(this),
				new AggieNode(this), new RandomNode(this), });
		nodeList.add(new Node[] { new GetWCstuff(this), new BankNodeWC(this), new WalkNodeWoodCutting(this),
				new WoodcuttingNode(this), new RandomNode(this), });
		nodeList.add(new Node[] { new GetChickenStuff(this), new WalkNodeChickens(this), new ChickensNode(this), });
		currTime = System.currentTimeMillis();
		choseNodes();
	}

	@Override
	public int onLoop() {
		// TODO Auto-generated method stub
		if (System.currentTimeMillis() - currTime > Calculations.random(1200000, 2400000)) {
			choseNodes();
		}

		for (Node node : currentNodes) {
			if (node.possible()) {
				if (node.validate()) {
					return node.execute();
				}
			} else {
				removeNodes(currentNodes);
			}
		}
		log("no node valid");

		return 1000;
	}

	public void removeNodes(Node[] nodes) {
		nodeList.remove(nodes);
		choseNodes();
	}

	public void choseNodes() {
		currentNodes = nodeList.get(Calculations.random(0, nodeList.size()));
		currTime = System.currentTimeMillis();
		MethodProvider.log("switching nodes");
	}

}
